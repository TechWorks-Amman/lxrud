# Viscosity Testing Equipment


![Img](final1.jpg)
## Tools needed

1. Syringe 60 ml
2. 3* Ball bearing 8mm
3. 2* linear guide 8mm
4. Threaded rod 8mm
5. Stepper motor (NEMA 17)
6. Acrylic sheet
7. Coupler


## Fabrication time and material usage
1. 6:30 on the Ultimaker s5
2. 44 grams of PLA
3. 10 minutes Laser cutter


## Design

We have used SolidWorks and Onshape 3D modeling software to design the Syringe pump. The design consist of 3 parts, the Stepper motor holder, syringe holder, and the syringe moving part.

All design files are available in TechWorks Gitlab project ( Lxrude)

## Assembly

Pictures to be added here

## Material ingredient & mixture

##### Clay:
- 100 gram of Clay
- 10 gram of water
- Isopropyl Alcohol
- 10 minute of mixing

#### Biomaterial:

## Material Viscosity

| Material  |Stepper movement    | Material Flow  |  material consistency  |  notes |
|---|---|---|---|---|
| Clay  | 10 mm  |   |   |   |
|   |   |   |   |   |
|   |   |   |   |   |


## Research outcome
